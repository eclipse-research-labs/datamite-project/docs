# Getting started

Take this guide mainly as a starting point for:

- Understand the structure of DATAMITE project repository.
- Make a contribution on existing project repository.
- Add a new project repository to DATAMITE.
- Recomendations about README files.
- Issue management and colaborative work.

## Project structure repository

The [main group](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project) is structured on subgroups according to each module from architecture diagram. Inside each module, the idea is to have code repositories for each of the components of the module.

This structure is based on the architecture diagram off the project.

## Contribute to the project

Follow the guidelines and recommendations of [CONTRIBUTING](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/docs/-/blob/main/CONTRIBUTING.md) file.

## Add new project repository to the project

Follow this steps to add your project repository to the right subgroup of DATAMITE project.

1. **Identify in which module the component whose repository you are going to upload is located**, and create a new project from within the subgroup corresponding to that module.

> **IMPORTANT**: _It is highly recommended that the name of the new project matches the name of the component to which it corresponds according to the architecture diagram_.
_For example, if it is a component belonging to data support tools, it should be hosted at [data support tools](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools) subgroup._

2. **In that menú, select the option that fits better** (create blank project, import project, etc.) **and follow the instructions.**


> _Following the previous example about the data discovery connectors (inside data support tools module), if you want to push an existing git repository from your local machine, use these commands:_
> ```
> cd existing_folder
> git init --initial-branch=main
> git remote add origin https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/data-discovery-connectors.git
> git add .
> git commit -m "Initial commit"
> git push --set-upstream origin main
> ```

## Recomendations about README files

It is recommended to include some of this sections on your README.md file when add new project repository to DATAMITE.

1. **Description**: Let people know what your project can do and provide some context about it.

2. **Screenshots or Diagrams**: As they say, a picture is worth a thousand words.

3. **Installation**: Consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible.

4. **Requirements**: Add this section if your project only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually.

5. **Usage**: This section is intended mainly to give examples or show some sort of expected output. It's helpful to have inline the smallest example of usage that you can demonstrate.

6. **Support**: It is important to have a contact person to address if there are questions or problems with the project.

7. **Roadmap**: If you have ideas for releases in the future, it is a good idea to list them in the README.

8. **License**: For open source projects, say how it is licensed.

## Issue management and colaborative work

**This project follows the standard rules of management of Gitlab Handbook**. You can find detailed information [here](https://handbook.gitlab.com/handbook/marketing/project-management-guidelines/issues/). 

Following forementioned Gitlab Handbook, the **following table has been used to estimate the issues workload** (by weight):

| Weight  | Time     |
| ------  | ---------|
|    0    | <4 hours |
|    1    | 4 hours  |
|    2    | 8 hours  |
|    3    | 12 hours |
|    4    | 16 hours |

Finally, you cand find default labels that you can use to manage the issues of your project. Feel free to create new ones if you need it. You can find the detailed information of the default labels [here](https://gitlab.eclipse.org/groups/eclipse-research-labs/datamite-project/-/labels)
