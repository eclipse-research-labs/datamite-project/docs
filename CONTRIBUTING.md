# CONTRIBUTION GUIDELINES

This section explains how to contribute to each repository of DATAMITE project.
Please, read this section carefully before making your contribution, especially if the purpose of your contribution is to update or provide source code. 

If you have any question, comment or problem, do not hesitate to contact us at:

* Jordi Arjona: jarjona@iti.es (repository's owner)
* Raul Costa: rcosta@iti.es 

For consistency purposes, the **proposed procedure** to contribute to this repository is as follows:

1. Create a new sub-branch of `main`. To allow a better identification of your contribution, it is strongly recommended to include the name of your institution in the branch's name.

2. Integrate your code with the already existing code.
 
3. Check that your scenario works. 

4. Make a merge request from your branch to `main` and set the repository owner as the reviewer. Please, include a short description of your contribution, including the new features it adds, or what it fixes or updates. 

5. Send an e-mail to the repository owner to notify about your merge request (to ensure that the merge request has been delivered)

6. Wait for the merge request's approval or improvement suggestions. Note that in the last case you will be requested to update your merge request. 

# RECOMMENDATIONS

The recommendations gathered below are based on good practices and are aimed at enhancing consistency and preventing common errors. 
We encourage to the contributors of the project to follow it.

1. Several small commits are preferred to a single large commit. Among other benefits, this approach facilitates the "readability" of your contribution.

2. When modifying an existing file, please, keep the indentation convention (tabs, n spaces, etc.). Currently, the indentation convention is using tabs, so this is the preferred option. However, if you add new files you may choose your preferred option.

3. Try to avoid committing debug code. If you have to do so, you may keep it as a comment or in a clear delimited section. In that case, it is strongly recommended to attatch a short comment explaining what this code does when being uncommented.

4. Check that your changes involve only files related to the component that you are contributing to.

5. Check your contribution does not upload unnecessary files. For instance, log files or intermediate files generated during a certain process. This can be enforced by adding the corresponding pattern in the `.gitignore` file.

Finally, thanks for your contribution!
