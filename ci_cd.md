# DESCRIPTION

This is a documentation providing the steps to setup a CI/CD pipeline with 3 jobs on 3 different stages.
The pipeline covers the stage of test -> build -> deploy


## General notes
The runner pipelines are already enabled on the Datamite Eclipse repository to verify you have the required permissions the menu 
build->pipelines should be availiable.

Runners are agents that run your CI/CD jobs.

To view available runners:
Go to Settings > CI/CD and expand Runners.
As long as you have at least one runner that’s active, with a green circle next to it, you have a runner available to process your jobs.

## Dockerhub
Have been setup on dockerhub an account for Datamite project in order we can upload/download from there our docker images.
After login(please email for the credentials) on dockerhub -> my profile -> Repositories you should see your project name opened as repository if does not exist we can create a new repository 
with the desired name like the pic.
![dockerhub image](images/dockerhub.png)

## Variables

CI/CD variables are a type of environment variable. You can use them to:

Control the behavior of jobs and pipelines.
Store values you want to re-use.
Avoid hard-coding values in your .gitlab-ci.yml file.
At the top level, it’s globally available and all jobs can use it.
In a job, only that job can use it.
You can add CI/CD variables to a project’s settings.

You must be a project member with the Maintainer role.
To add or update variables in the project settings:

On the left sidebar, select Search or go to and find your project.
Select Settings > CI/CD.
Expand Variables.
Select Add variable and fill in the details:
Key: Must be one line, with no spaces, using only letters, numbers, or _.
Value: No limitations.
Type: Variable (default) or File.
Environment scope: Optional. All (default) (*), a specific environment, or a wildcard environment scope.
Protect variable Optional. If selected, the variable is only available in pipelines that run on protected branches or protected tags.
Mask variable Optional. If selected, the variable’s Value is masked in job logs. The variable fails to save if the value does not meet the masking requirements.

Before the gitlab-ci.yml will need to define some variables on project’s settings -> CI/CD variables. We must define the credentials of our dockerhub account and the configuration of the ITI vm
(ip of the vm, port, the vm user and the ssh key ) like the pic. (please email for the values)
![variables image](images/variables.png)


## gitlab-ci.yml

The starting point is to create a .gitlab-ci.yml file in the root of your directory. It is a YAML file where you specify instructions for GitLab CI/CD.

In this file, you define:

The structure and order of jobs that the runner should execute.
The decisions the runner should make when specific conditions are encountered.
With each push or merge the pipeline will be triggered to be executed if this is not desired  add the [ci skip] keyword to your commit message to make CI/CD skip the pipeline.

Below is a working example of building a docker image for the logging tool, pushing the image to dockerhub and then triggering an other pipeline on ITI project to download the image to a deployment vm.



--------------------------------------------------------------------------------------------------------------------
variables:
  KUBERNETES_PRIVILEGED: "true"
  DOCKER_BUILDKIT: 1
  BUILDKIT_PROGRESS: plain
  DOCKER_CLI: '1'
  CI_PROJECT_DIR:  "."  


stages:
  - build
  - test
  - deploy
  - notify

include:
  - project: 'eclipsefdn/it/releng/gitlab-runner-service/gitlab-ci-templates'
    file: '/jobs/buildkit.gitlab-ci.yml'


buildkit:
  extends: .buildkit
  variables:
      CI_REGISTRY_IMAGE: datamite/logging

  
notify_update:
  stage: notify
  trigger:
    project: eclipse-research-labs/datamite-project/data-support-tools/data_ingestion_and_storage/infrastructure

  
---------------------------------------------------------------------------------------------------------------------------  
This example uses the builtkit tool to build a docker image from the dockerfile -> push it on the dockerhub repository datamite/logging and then trigger an other job on ITI project (https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/data_ingestion_and_storage/infrastructure/-/blob/main/.gitlab-ci.yml?ref_type=heads) to remotely login on a deploy vm and download the image there.

The link for the gitlab-ci.yml file is https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/logging-tool/-/blob/main/.gitlab-ci.yml?ref_type=heads


 
## View the status of your pipeline and jobs

1.Go to Build > Pipelines. A pipeline with three stages should be displayed:
2.View a visual representation of your pipeline by selecting the pipeline ID:
3.View details of a job by selecting the job name. For example, deploy-prod:



